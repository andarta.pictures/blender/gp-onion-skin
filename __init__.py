# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import bpy
from . import (
    ops,
    ui,
)

bl_info = {
    "name" : "GP_OnionSkin",
    "author" : "Tom VIGUIER, Sophie NOERIE",
    "description" : "Enhanced Onion Skin tab, providing world relative, transformable onion skin instances",
    "blender" : (3, 6, 0),
    "version" : (0, 1, 4.01),
    "location" : "N-Panel in 3D view",
    "warning" : "This tool is in early development stage, thank you for giving it a try ! please report any bug to t.viguier@andarta-pictures.com",
    "category" : "Andarta",
    "tracker_url": "https://gitlab.com/andarta-pictures/gp-onion-skin/-/issues"
}


classes = [
    ]

def register():
    for cls in classes :
        bpy.utils.register_class(cls)

       
def unregister():
    #remove handlers

    for cls in reversed(classes) :
        bpy.utils.unregister_class(cls)



modules = [
    ops, 
    ui, 
]

def register():
    for m in modules :
        m.register()

def unregister():
    for m in modules :
        m.unregister()
 
if __name__ == "__main__":
    register()
