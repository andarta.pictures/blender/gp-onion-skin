'''Core function of GP_OS'''
import time
import bpy
from bpy.app.handlers import persistent
from .common.hash_utils import has_hash_change
from .common.general_utils import conform_object, duplicate_object, get_objects_childs_dic
from .common.gp_utils import get_gp_layername_list, get_gplyr_from_name, get_material_by_name, get_now_gpframe, stroke_confo
from mathutils import Vector, Matrix


#default values for ghosts opacities
ghost_opacity_gradient = (0.08,0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.45, 0.55, 0.70,
                           1.00, 
                           0.70, 0.55, 0.45, 0.35, 0.30, 0.25, 0.20, 0.15, 0.10,0.08)
ghost_prefix = 'GHOST_'

'''GHOST FUNCTIONS'''

def get_ghost(src_ob):
    """Gets the corresponding ghost object from an object.

    Arg:
        src_ob (Object): object for which we want the corresponding ghost

    Returns:
        Object: ghost object if found
        None: if corresponding ghost not found
    """
    ob = None
    if src_ob is not None:
        OScollection = bpy.data.collections.get("ghost_collection")
        if OScollection is not None:
            ob = OScollection.objects.get(ghost_prefix + src_ob.name)
    return ob

def generate_obj_ghost(src_ob,
                       displayed_frames_before ,
                       displayed_frames_after,
                       verbose = True,
                       individual_set_coord = False
                       ) :
    """Generates onion skin ghosts object and collection.

    Arg:
        src_ob (Object): object for which we want to generate a ghost
        displayed_frames_before (list of int): list of frame numbers of frames to display before current frame
        displayed_frames_after (list of int): list of frame numbers of frames to display after current frame
        verbose (bool, optional): if True, prints info in console. Defaults to True.
        individual_set_coord (bool, optional): if True, sets coordinates of ghost object. SLOW prefer bulk set. Defaults to False.
    """
    start_time = time.time()
    # if is_native_os_enabled() is False:
    #      return
    active_obj = bpy.context.object
    cur_fr = bpy.context.scene.frame_current
    scene = bpy.context.scene
    os_ui_data = scene.osdata
    os_ui_data.cur_fr = cur_fr

    if src_ob.type == 'GPENCIL' :

        active_obj_layer = None
        if active_obj is not None and active_obj.type == 'GPENCIL':
            active_obj_layer = active_obj.data.layers.active

        #look at existing ghost collection
        OScollection = bpy.data.collections.get("ghost_collection")
        if OScollection is None:
            OScollection = bpy.data.collections.new("ghost_collection")
            OScollection.hide_render = True
            scene.collection.children.link(OScollection)

        tgt_ob =  OScollection.objects.get(ghost_prefix + src_ob.name)
        
        if tgt_ob is not None:
            # clear_obj_ghost(src_ob)
            tgt_gp_data = tgt_ob.data
            pass
        else:  
            #Ghost Object settings
            # tgt_ob = src_ob.copy()
            tgt_ob = duplicate_object(src_ob)
            #remove modifiers and constraints from tgt
            tgt_ob.modifiers.clear()
            tgt_ob.constraints.clear()

            tgt_ob.name = ghost_prefix + src_ob.name
            tgt_ob.data = bpy.data.grease_pencils.new(ghost_prefix + src_ob.data.name)
            tgt_gp_data = tgt_ob.data
            if os_ui_data.before_material is None:
                os_ui_data.before_material =get_material_by_name(name=os_ui_data.before_material_name, 
                                                                 create = True,stroke=True,fill=False,
                                                                 default_color= os_ui_data.cur_before_color
                                                                    )
                os_ui_data.after_material = get_material_by_name(name=os_ui_data.after_material_name,
                                                                    create = True,stroke=True,fill=False,
                                                                    default_color= os_ui_data.cur_after_color
                                                                    )
                os_ui_data.before_fill_material = get_material_by_name(name=os_ui_data.before_fill_material_name,
                                                                    create = True,stroke=False,fill=True,
                                                                    default_color= os_ui_data.cur_fill_before_color
                                                                    )
                os_ui_data.after_fill_material = get_material_by_name(name=os_ui_data.after_fill_material_name,
                                                                    create = True,stroke=False,fill=True,
                                                                    default_color= os_ui_data.cur_fill_after_color
                                                                    )
            #remove all existing mats in tgt_gp_data
            for mat in tgt_gp_data.materials:
                tgt_gp_data.materials.remove(mat)
            tgt_gp_data.materials.append(os_ui_data.before_material)
            tgt_gp_data.materials.append(os_ui_data.after_material)
            tgt_gp_data.materials.append(os_ui_data.before_fill_material)
            tgt_gp_data.materials.append(os_ui_data.after_fill_material)


            OScollection.objects.link(tgt_ob)
            #FROm OLD COORDsET
            tgt_ob.animation_data_clear()
            tgt_ob.parent = None
            tgt_ob.location = [0,0,0]
            tgt_ob.rotation_euler = [0,0,0]
            tgt_ob.scale = [1,1,1]

       
        # if os_ui_data.ghost_in_front:
        #     tgt_ob.show_in_front = True
        #     # tgt_ob.location = [0,-0.02,0]
        # else: 
        #     # tgt_ob.location = [0,0.02,0]
        #     tgt_ob.show_in_front = False
        tgt_ob.hide_select = True
        tgt_ob.hide_render = True

        #ONION SKIN SET:
        

        ##source grease pencil settings
        src_gp_data = src_ob.data
        src_gp_data_cur_layer = src_gp_data.layers.active
        os_ui_data.src_onion_factor = src_gp_data.onion_factor
        # os_ui_data.src_ghost_before_range = src_gp_data.ghost_before_range
        # os_ui_data.src_ghost_after_range = src_gp_data.ghost_after_range
        src_gp_data.onion_factor = 0.0
        src_gp_data.ghost_before_range = 0
        src_gp_data.ghost_after_range = 0
        src_gp_data.os_src_ob_data.src_in_front_value = src_ob.show_in_front #Use to force put in front
        if src_gp_data.os_src_ob_data.parent_name =="":
             src_gp_data.os_src_ob_data.parent_name = src_ob.name

        src_ob.show_in_front = os_ui_data.force_in_front

        ##ghost grease pencil settings
        
        tgt_gp_data.onion_keyframe_type = src_gp_data.os_src_ob_data.filter_kf_type
        tgt_gp_data.onion_factor = os_ui_data.cur_onion_factor
        tgt_gp_data.onion_mode = src_gp_data.onion_mode #os_ui_data.cur_onion_mode
        tgt_gp_data.ghost_before_range = os_ui_data.ghost_before_range #src_gp_data.os_src_ob_data.ghost_before_range
        tgt_gp_data.ghost_after_range = os_ui_data.ghost_after_range #src_gp_data.os_src_ob_data.ghost_after_range
        tgt_gp_data.before_color = os_ui_data.cur_before_color
        tgt_gp_data.after_color = os_ui_data.cur_after_color
        # tgt_gp_data.os_current_ghost_opacities = ghost_opacity_gradient#src_gp_data.os_current_ghost_opacities
        tgt_gp_data.use_onion_fade = False
        if verbose:
            print("Settings set in %s seconds" % (time.time() - start_time))
            start_time = time.time()

        create_src_frame_as_ghost_layer(src_ob, tgt_ob, 
                                        frames_index_lists= displayed_frames_before + displayed_frames_after, 
                                        current_frame=cur_fr,
                                        #verbose=verbose,
                                        )
        if verbose:
            print("Layers created in %s seconds" % (time.time() - start_time))
            start_time = time.time()
        new_update_ghost_layer_opacity(tgt_ob.data, bpy.context)
        if verbose:
            print("Opacity updated in %s seconds" % (time.time() - start_time))
            start_time = time.time()

        # update_custom_layer_opacity(tgt_ob.data, bpy.context)

        ## apply transformations to ghost object
        if individual_set_coord:
            start_time = time.time()
            set_coordinates(src_ob, tgt_ob, scene, cur_fr, 
                            visible_ghosts=displayed_frames_before + displayed_frames_after
                            )
            if verbose: print("OLD Coordinates set in %s seconds" % (time.time() - start_time))

        ##save data and restore context
        # for obj in bpy.context.selected_objects:
        #     obj.select_set(False)
        # try :
        #     active_obj.select_set(True)
        #     bpy.context.view_layer.objects.active = active_obj
        #     if active_obj_layer is not None:
        #         active_obj.data.layers.active = active_obj_layer
        # except (ReferenceError, AttributeError) :
        #     pass
        return tgt_ob

def clear_obj_ghost(src_ob) :
    """Clears the ghost corresponding to a given object.

    Arg:
        src_ob (Object): source object of the ghost to clear
    """
    if src_ob is None or src_ob.type != 'GPENCIL':
        return
    #restore initial settings
    scene = bpy.context.scene
    os_ui_data = scene.osdata
    src_gp_ob = src_ob.data
    src_gp_ob.onion_factor = os_ui_data.src_onion_factor
    # src_gp_ob.ghost_before_range = os_ui_data.src_ghost_before_range 
    # src_gp_ob.ghost_after_range = os_ui_data.src_ghost_after_range
    # src_ob.show_in_front = src_gp_ob.os_src_ob_data.src_in_front_value
            
    ob = get_ghost(src_ob)
    if ob is None :
        return
    gp_ob = ob.data
    os_ui_data.cur_onion_factor = gp_ob.onion_factor
    os_ui_data.cur_onion_mode =gp_ob.onion_mode
    # os_ui_data.cur_ghost_before_range = gp_ob.ghost_before_range
    # os_ui_data.cur_ghost_after_range = gp_ob.ghost_after_range
    os_ui_data.cur_before_color = gp_ob.before_color
    os_ui_data.cur_after_color = gp_ob.after_color  

    OScollection = bpy.data.collections["ghost_collection"]

    #remove ghost object
    bpy.data.objects.remove(ob)
    bpy.data.grease_pencils.remove(gp_ob)

    #if OScollection is Empty 
    objs = []
    for obj in OScollection.all_objects:
        objs.append(obj)
    if objs == [] :
        bpy.data.collections.remove(OScollection)

def update_ghost_collection(verbose = True,refresh = False):
    """Updates all ghosts."""
    #Time control
    if verbose: 
        print("Updating Ghosts")
        start_time = time.time()

    cible = {}
    selected={}
    for obj in bpy.context.selected_objects:
        selected[obj.name] = obj

    os_data = bpy.context.scene.osdata
    for pinned_obj_key in os_data.get_pinned_objects_keys():
        if pinned_obj_key not in selected.keys():
            #get the pinned object
            pinned_obj = bpy.data.objects.get(pinned_obj_key)
            if pinned_obj is not None:
                #if not selected, add it to the list of selected objects                        
                selected[pinned_obj_key]=pinned_obj
    #Gather expected ghost objects
    match os_data.target :
        case 'SELECTED' :
            cible = selected
        case 'CHILD' : 
            cible= get_objects_childs_dic(list(selected.values()),bpy.data.objects,add_self=True,recursive_dic={})
            #TIME CONTROL
            if verbose: 
                print("Children gathered in %s seconds" % (time.time() - start_time))
                start_time= time.time()



    OScollection = bpy.data.collections.get("ghost_collection")
    if OScollection is None:
        OScollection = bpy.data.collections.new("ghost_collection")
        OScollection.hide_render = True
        bpy.context.scene.collection.children.link(OScollection)


    if OScollection is not None:
        #make a copy of objects to avoid unwanted loop
        existing_ghosts_keys = OScollection.objects.keys()
        for k in existing_ghosts_keys:
            #search original object of the ghost
            src_key =k[6:]
            if src_key not in cible.keys():
                #hide this gghost object in vewiport
                OScollection.objects[k].hide_viewport = True
                # set src object show_in_front
                src_ob = bpy.data.objects.get(src_key)
                if src_ob is not None:
                    src_ob.show_in_front = False
                
            else:
                OScollection.objects[k].hide_viewport = False
                
        #get frame cible
        current_frame = bpy.context.scene.frame_current
        if os_data.onion_mode =='Frames':
            frames_before = [current_frame - (i+1) for i in range(os_data.ghost_before_range)]
            frames_after = [current_frame + (i+1) for i in range(os_data.ghost_after_range)]
        else:
            frames_before = []
            frames_after = []
            for src_ob in selected.values():   
                # src_key =k[6:]
                # src_ob = bpy.data.objects.get(src_key)
                if src_ob is None:
                    continue
                frames_before += get_frames_number_before(src_ob, current_frame, os_data.ghost_before_range)        
                frames_after += get_frames_number_after(src_ob, current_frame, os_data.ghost_after_range)
            #get the n closer frames to current frame in frame before and after
            frames_before = sorted(frames_before, key=lambda x:abs(x-current_frame))[:os_data.ghost_before_range]
            frames_after = sorted(frames_after, key=lambda x:abs(x-current_frame))[:os_data.ghost_after_range]
        #generate two list of 32 values frames_before[i] if in index range else 0
        os_data.displayed_frames_after =frames_after + [0 for i in range(10-len(frames_after))] 
        os_data.displayed_frames_before =frames_before+ [0 for i in range(10-len(frames_before))] 

        ghost_list = []
        for obj in cible.values():
            ghost_list.append(generate_obj_ghost(obj,frames_before,frames_after)) #generate a new ghost

        if verbose: 
            print("Ghosts updated in %s seconds" % (time.time() - start_time))
            start_time= time.time()

        if os_data.ghost_in_front:
            z_offset = (0,-0.09,0)
            
        else:
            z_offset = (0,0.09,0)
        #turn tuple into vector
        z_offset = Vector(z_offset)
        faster_set_coordinates(list(cible.values()), 
                               ghost_list, 
                               bpy.context.scene, 
                               frames_before+frames_after,
                               z_offset=z_offset)
        if current_frame!=bpy.context.scene.frame_current:
            bpy.context.scene.frame_set(current_frame)
        
        if verbose: 
            print("coord updated in %s seconds" % (time.time() - start_time))
            refreshing_time = time.time()
        if refresh:
            refresh_dopesheets()
            if verbose: print("Dopesheets refreshed in %s seconds" % (time.time() - refreshing_time))

#Whatfor why not just adjust framerange and update?
# def add_frames_to_ghost(src_ob, ghost, mode, farthest_frame_nb, offset, context):
#     """Completes a given ghost by adding new frames to it.

#     Args:
#         src_ob (Object): object used as reference for ghost update
#         ghost (Object): ghost object to modify
#         mode (str): onion skin mode
#         farthest_frame_nb (int): frame number before or after which one we want to add frames
#         offset (int): number of frames to add
#         context (Context): context to use
#     """
#     #create a temporary object to prepare data to add to ghost
#     temp = src_ob.copy()
#     temp.name = 'temp_ghost'
#     temp.data = bpy.data.grease_pencils.new('temp_ghost')
#     context.collection.objects.link(temp)
#     #identify frames to add and copy them to temporary object
#     if mode == "before":
#         frames_to_add = get_frames_number_before(src_ob, farthest_frame_nb, offset)
#         complete_with_frames_before_after(src_ob, temp, frames_to_add)
#     elif mode == "after":
#         frames_to_add = get_frames_number_after(src_ob, farthest_frame_nb, offset)
#         complete_with_frames_before_after(src_ob, temp, frames_to_add)
#     #add new frames to ghost
#     create_layer_per_frame(temp, ghost, context.scene.frame_current, False)
#     #complete with blank keyframes for new layers on other previous displayed frame numbers
#     for nb in frames_to_add:
#         layer = ghost.data.layers[str(nb)]
#         all_displayed_frames = list(ghost.data.os_ghost_data["displayed_frames_before"]) + list(ghost.data.os_ghost_data["displayed_frames_after"])
#         for frame_nb in all_displayed_frames:
#             layer.frames.new(frame_nb)
#         layer.frames.new(context.scene.frame_current)
#     #apply transformation to have correct coordinates
#     set_coordinates(src_ob, ghost, context.scene, context.scene.frame_current)
#     update_custom_layer_opacity(ghost.data, context) #update opacity of layers
#     #update list of displayed frames in ghost data
#     if mode == "before":
#         ghost.data.os_ghost_data["displayed_frames_before"] = frames_to_add + list(ghost.data.os_ghost_data["displayed_frames_before"])
#     elif mode == "after":
#         ghost.data.os_ghost_data["displayed_frames_after"] = list(ghost.data.os_ghost_data["displayed_frames_after"]) + frames_to_add
#     bpy.data.grease_pencils.remove(temp.data) #delete temporary object

# def new_add_frame_to_ghost(src_ob, ghost, mode, farthest_frame_nb, offset, context):
#         """Completes a given ghost by adding new frames to it.

#     Args:
#         src_ob (Object): object used as reference for ghost update
#         ghost (Object): ghost object to modify
#         mode (str): onion skin mode
#         farthest_frame_nb (int): frame number before or after which one we want to add frames
#         offset (int): number of frames to add
#         context (Context): context to use
#     """

def remove_frames_from_ghost(ghost, frames_to_remove):
    """Removes frames from a ghost.

    Args:
        ghost (Object): object where to remove frames
        frames_to_remove (list of int): list of frame numbers of frames to remove
    """
    for frame_nb in frames_to_remove:
        #remove layer representing this frame if it exists
        try:
            ghost.data.layers.remove(ghost.data.layers[str(frame_nb)])
        except KeyError:
            pass
        #remove keyframes on other layers for this frame number
        for layer in ghost.data.layers:
            frame = get_now_gpframe(frame_nb, layer)
            if frame is not None:
                layer.frames.remove(frame)

def new_update_ghost_layer_opacity(self, context):
    '''Updates visibility and opacity of ghost layers according to current frame and ghost opacity gradient.
    args:
        self (GreasePencil): object to update
        context (Context): context to use
    '''
    if self.name.startswith(ghost_prefix):
        current_frame = context.scene.frame_current    
        # index_range = [current_frame-self.ghost_before_range,current_frame+self.ghost_after_range]
        #get os data current onion factor
        os_ui_data = context.scene.osdata
        current_onion_factor = os_ui_data.cur_onion_factor
        displayed_frames_before = list(os_ui_data.displayed_frames_before)[0:os_ui_data.ghost_before_range]
        displayed_frames_after = list(os_ui_data.displayed_frames_after)[0:os_ui_data.ghost_after_range]
        #itertae trough each layers of object
        for layer in self.layers:
            # get layers.info index in os_ui_data.displayed_frames_before list
            #gt index center
            index = int(len(os_ui_data.ghost_opacity_gradient) / 2)
            if int(layer.info) in displayed_frames_before:
                layer.hide = False
                offset = displayed_frames_before.index(int(layer.info))+1
                index -= offset
                #get opacity value
                layer.opacity  = os_ui_data.ghost_opacity_gradient[index]*current_onion_factor
            elif int(layer.info) in displayed_frames_after:
                layer.hide = False
                index += displayed_frames_after.index(int(layer.info))+1
                #get opacity value
                layer.opacity  = os_ui_data.ghost_opacity_gradient[index]*current_onion_factor
            else:
                #layer is not in displayed frames
                #hide it
                layer.hide = True
                continue

            # #if out of range>hide
            # if int(layer.info) < index_range[0] or int(layer.info) > index_range[1]:
            #     layer.hide = True
            # else:
            #     layer.hide = False
            #     #Calculate index
            #     if self.onion_mode=='RELATIVE':
            #         pass
            #     elif self.onion_mode == 'ABSOLUTE':
            #         pass
            #     middle_frame = len(ghost_opacity_gradient) / 2
            #     index = int(int(layer.info) - current_frame + middle_frame)
            #     #get opacity value
            #     if index<=len(ghost_opacity_gradient):
            #         layer.opacity  = ghost_opacity_gradient[index]*current_onion_factor
            #     else:
            #         layer.opacity = 0.08*current_onion_factor

                
                
                    


                
# OLD
# def update_custom_layer_opacity(self, context):
#     """Updates layer opacity values of a ghost object, according to stored opacities.

#     Args:
#         self (GreasePencil): object to update
#         context (Context): context to use
#     """
#     if self.name.startswith(ghost_prefix):
#         src_gp_ob = bpy.data.grease_pencils.get(self.name[6:])
#         if src_gp_ob is not None:
#             src_gp_ob.os_current_ghost_opacities = self.os_current_ghost_opacities #update src object opacities data
#         if len(self.layers) > 0:
#             opacity_list_center_index = len(ghost_opacity_gradient) / 2
#             current_kf_index = get_kf_index_from_nb(context.scene.frame_current,self.layers[0]) #get index of the current frame
#             for layer in self.layers:
#                 kf_index = get_kf_index_from_nb(int(layer.info),layer) #get index of the frame represented by this layer
#                 if current_kf_index is not None :
#                     index_opacity = -1
#                     #compute index which could give the opacity wanted for this layer
#                     if kf_index < current_kf_index:
#                         index_opacity = int(opacity_list_center_index - (current_kf_index - kf_index))

#                     elif kf_index > current_kf_index:
#                         index_opacity = int(opacity_list_center_index - 1 + (kf_index - current_kf_index))

#                     #update the layer opacity
#                     if 0 <= index_opacity < len(ghost_opacity_gradient):
#                         #opacity can be taken in the opacity list of the object
#                         layer.opacity = self.os_current_ghost_opacities[index_opacity]
#                     else:
#                         #opacity cannot be taken from opacity list of the object
#                         layer.opacity = 0.08

def update_filter_kf_type(self, context):
    """Generate again existing ghosts of active object or active collection, using new value of filter_kf_type."""
    os_ui_data = context.scene.osdata
    if os_ui_data.stop_recursion is True:
        #avoid infinite recursion
        return
    #update active object, regardless of the mode
    
    src_ob = context.object
    src_ob.data.onion_keyframe_type = self.filter_kf_type
    ghost = get_ghost(src_ob)
    if ghost is not None:
        generate_obj_ghost(src_ob)
    if os_ui_data.target == 'collection':
        #update all ghosts of the active collection, with filter_kf_type value of active object
        os_ui_data.stop_recursion = True #to avoid infinite recursion
        #make a copy of all_objects to avoid unwanted loop
        listObj = context.collection.all_objects.values()
        for obj in listObj:
            if obj.type != 'GPENCIL' :
                continue
            obj.data.os_src_ob_data.filter_kf_type = self.filter_kf_type
            obj.data.onion_keyframe_type = self.filter_kf_type
            ghost = get_ghost(obj)
            if ghost is not None:
                generate_obj_ghost(obj)
        os_ui_data.stop_recursion = False
    refresh_dopesheets()

def op_generate(context, mode):
    """Updates onion skin mode and generate wanted ghost(s).

    Args:
        context (Context): context to use
        mode (str): new onion skin mode
    """
    context.area.spaces[0].overlay.show_overlays = True
    if not is_native_os_enabled() :
        #get 3D view spaces
        space3D = []
        for area in context.screen.areas :
            if area.type == "VIEW_3D":
                #spaces[0] is current space
                space3D.append(area.spaces[0])
                break
        if not space3D:
            return{'FINISHED'}

        #update data
        for space in space3D:        
            space.overlay.use_gpencil_onion_skin = True

    os_ui_data = context.scene.osdata
    
    # if os_ui_data.mode != mode:
    #     os_ui_data.mode = mode
    #clear the list of backups for frame transforms
    OScollection = bpy.data.collections.get("ghost_collection")
    if OScollection is not None :
        for ghost in OScollection.all_objects:
            src_ob = bpy.data.objects.get(ghost.name[6:])
            if src_ob is not None:
                src_ob.data.os_src_ob_data.list_matrix_transform.clear()
    #generate again all existing ghosts with the new mode
    update_ghost_collection()
    # generate_obj_ghost(context.object)

    # if os_ui_data.target == 'object':
    #     generate_obj_ghost(context.object)
    # elif os_ui_data.target == 'collection':
    #     #make a copy of all_objects to avoid unwanted loop
    #     listObj = context.collection.all_objects.values()
    #     for obj in listObj:
    #         generate_obj_ghost(obj)
    # refresh_dopesheets()


'''CALLBACKS FUNCTIONS'''

@persistent
def frame_handler(scene):
    """Called on frame change: updates all ghosts.

    Arg:
        scene (Scene): scene where update is needed
    """
    if  scene.osdata.mode == 'DIS' or not bpy.context.screen or scene.osdata.semaphore == True :#or not is_native_os_enabled()
        #addon onion skin disabled or no current screen
        return
    scene.osdata.semaphore =True
    OScollection = bpy.data.collections.get("ghost_collection")

    if bpy.context.screen.is_animation_playing:
        #playing animation : doesn't need to refresh or see onion skin
        if OScollection is not None and not OScollection.hide_viewport:
            OScollection.hide_viewport = True
        scene.osdata.semaphore = False
        return

    if OScollection is not None:
        #refresh ghosts
        if  OScollection.hide_viewport : #is_native_os_enabled() and 
            #display ghost collection if needed
            
            OScollection.hide_viewport = False
        update_ghost_collection()
    # print("Frame Change", scene.frame_current)
    scene.osdata.semaphore = False

@persistent
def pre_render(scene):
    """Called on render beginning: remove frame change handler to prevent update of ghosts during render.

    Arg:
        scene (Scene): scene where update is needed
    """
    #remove frame change handler
    while frame_handler in bpy.app.handlers.frame_change_post :
        bpy.app.handlers.frame_change_post.remove(frame_handler)

@persistent
def post_render(scene):
    """Called after render: restore frame change handler.

    Arg:
        scene (Scene): scene where update is needed
    """
    #add frame change handler
    bpy.app.handlers.frame_change_post.append(frame_handler)


last_objs = None



@persistent
def scene_update_post(scene):
    """Called after scene update: updates all ghosts.

    Arg:
        scene (Scene): scene where update is needed
    """
    global  last_objs

    if not bpy.context.screen or scene.osdata.mode == 'DIS'or scene.osdata.refresh_on_select == False or scene.osdata.semaphore == True :
        #addon onion skin disabled or no current screen
        return
    
    if last_objs == bpy.context.selected_objects:
        return
        
    # last_length = len(bpy.context.selected_objects)
    last_objs = bpy.context.selected_objects

    scene.osdata.semaphore =True
    OScollection = bpy.data.collections.get("ghost_collection")

    if bpy.context.screen.is_animation_playing:
        #playing animation : doesn't need to refresh or see onion skin
        if OScollection is not None and not OScollection.hide_viewport:
            OScollection.hide_viewport = True
        scene.osdata.semaphore = False
        return

    if OScollection is not None:
        #refresh ghosts
        if  OScollection.hide_viewport : #is_native_os_enabled() and 
            #display ghost collection if needed
            OScollection.hide_viewport = False
        update_ghost_collection()
    # print("Frame Change", scene.frame_current)
    scene.osdata.semaphore = False

# def select_assembly(scene):
# 	global last_length, need_to_update


# 	if last_length != len(bpy.context.selected_objects):
# 		need_to_update = True
# 		last_length = len(bpy.context.selected_objects)


# 	if need_to_update == True:
# 		print("Updated:", time.time())
# 		need_to_update = False
# def add_handler(handlers, func):
# 	c = 0
# 	r = False
# 	for i in handlers:
# 		if i.__name__ == func.__name__:
# 			handlers.remove(handlers[c])
# 			r = True
# 		c += 1
# 	handlers.append(func)
# 	print(("Added" if r == False else "Replaced")+" Handler:", i.__name__)
# add_handler(handlers, select_assembly)


'''GENERAL FUNCTIONS'''#may need to be moved or replaced  by appropriate common
'''--COORDINATE FUNCTIONS--'''
def set_coordinates(src_ob, tgt_ob, scene, cur_fr,visible_ghosts = [0]):
    """Positions frames of given ghost, according to current onion skin mode.

    Args:
        src_ob (Object): source object of ob (object used as reference)
        tgt_ob (Object): ghost to update
        scene (Scene): current scene
        cur_fr (int): frame number of current frame / current instant on timeline
        visible_ghosts (list of int, optional): list of frame numbers of visible ghosts. Defaults to [0].
    """
    #remove frame change handler
    
    os_ui_data = scene.osdata
    mode =  os_ui_data.mode
    if mode == 'CAM' and scene.camera is None:
        mode = 'REL'
    
    tgt_ob.animation_data_clear()
    tgt_ob.parent = None
    tgt_ob.location = [0,0,0]
    tgt_ob.rotation_euler = [0,0,0]
    tgt_ob.scale = [1,1,1]
    
    
    #save current world matrix of active camera
    
    for tgt_layer in tgt_ob.data.layers :
        if int(tgt_layer.info) not in visible_ghosts:
            continue

        backup = src_ob.data.os_src_ob_data.list_matrix_transform.get(str(tgt_layer.info)) 
        if backup is not None:
            #backup transform data exists, use it
            tgt_layer.location= backup.location
            tgt_layer.rotation = backup.rotation
            tgt_layer.scale = backup.scale
            continue
        else:
            match mode:
                case 'ABS' :
                    # set_coordinates_world_mode(src_ob, ob, scene, cur_fr)          
                    
                    scene.frame_set(int(tgt_layer.info))
                    loc, rot, sca = src_ob.matrix_world.decompose()
                    tgt_layer.location= loc
                    tgt_layer.rotation = rot.to_euler()
                    tgt_layer.scale = sca
                    scene.frame_set(cur_fr)

                    
                case 'REL':
                    # set_coordinates_object_mode(src_ob, ob, scene, cur_fr)    
                    loc, rot, sca = src_ob.matrix_world.decompose()        
                    tgt_layer.location= loc
                    tgt_layer.rotation = rot.to_euler()
                    tgt_layer.scale = sca
                    
                case 'CAM':
                    # set_coordinates_camera_mode(src_ob, ob, scene, cur_fr)
                    scene.frame_set(int(tgt_layer.info))
                    #get reference data
                    cam_mat_current = scene.camera.matrix_world.copy()
                    cam_mat_kf_invert = scene.camera.matrix_world.inverted()
                    old_glob = src_ob.matrix_world #old global data
                    local = cam_mat_kf_invert @ old_glob #local data in camera coordinate system, corresponding to old global
                    new_glob = cam_mat_current @ local #new global data : compute from local data according to current camera world_matrix
                    #set new calculated data
                    loc, rot, sca = new_glob.decompose()
                    tgt_layer.location= loc
                    tgt_layer.rotation = rot.to_euler()
                    tgt_layer.scale = sca
                    scene.frame_set(cur_fr)
        

    # while frame_handler in bpy.app.handlers.frame_change_post :
    #     bpy.app.handlers.frame_change_post.remove(frame_handler)
    # try:
    #     os_ui_data = scene.osdata
    #     match os_ui_data.mode:
    #         case 'ABS' :
    #             set_coordinates_world_mode(src_ob, ob, scene, cur_fr)
    #         case 'REL':
    #             set_coordinates_object_mode(src_ob, ob, scene, cur_fr)
    #         case 'CAM':
    #             set_coordinates_camera_mode(src_ob, ob, scene, cur_fr)
    # finally:
    #     #add frame change handler
    #     bpy.app.handlers.frame_change_post.append(frame_handler)

def faster_set_coordinates(src_obj_list, tgt_obj_list, scene, ghosted_frames = [0],z_offset = None):
    os_ui_data = scene.osdata
    mode =  os_ui_data.mode
    if mode == 'CAM' and scene.camera is None:
        mode = 'REL'

    

    for frame in ghosted_frames:
        if mode == 'ABS' or mode == 'CAM':
            scene.frame_set(frame)
        for i in range(len(src_obj_list)):
            src_ob = src_obj_list[i]
            

            tgt_ob = tgt_obj_list[i]  
            if z_offset is not None:
                tgt_ob.location = z_offset
            #get this out of here! 
            # tgt_ob.animation_data_clear()
            # tgt_ob.parent = None
            # tgt_ob.location = [0,0,0]
            # tgt_ob.rotation_euler = [0,0,0]
            # tgt_ob.scale = [1,1,1]

            tgt_layer = tgt_ob.data.layers.get(str(frame))
            if not tgt_layer:
                continue         
            backup = src_ob.data.os_src_ob_data.list_matrix_transform.get(str(frame)) 
            
            if backup is not None:
                #backup transform data exists, use it
                tgt_layer.location= backup.location
                tgt_layer.rotation = backup.rotation
                tgt_layer.scale = backup.scale
                continue
            else:   
                if mode in ['ABS','REL']:  
                    loc, rot, sca = src_ob.matrix_world.decompose()    
                elif mode == 'CAM':
                    #get reference data
                    cam_mat_current = scene.camera.matrix_world.copy()
                    cam_mat_kf_invert = scene.camera.matrix_world.inverted()
                    old_glob = src_ob.matrix_world  
                    local = cam_mat_kf_invert @ old_glob #local data in camera coordinate system, corresponding to old global
                    new_glob = cam_mat_current @ local #new global data : compute from local data according to current camera world_matrix
                    #set new calculated data
                    loc, rot, sca = new_glob.decompose()
                tgt_layer.location = loc
                tgt_layer.rotation = rot.to_euler()
                tgt_layer.scale = sca
                

# def set_coordinates_object_mode(src_ob, ob, scene, cur_fr):
#     """Positions frames of given ghost according to current position of source object.

#     Args:
#         src_ob (Object): source object of ob (object used as reference)
#         ob (Object): ghost to update
#         scene (Scene): current scene
#         cur_fr (int): frame number of current frame / current instant on timeline
#     """
#     ob.animation_data_clear()
#     ob.parent = None
#     ob.location = [0,0,0]
#     ob.rotation_euler = [0,0,0]
#     ob.scale = [1,1,1]
#     scene.frame_set(cur_fr)
#     loc, rot, sca = src_ob.matrix_world.decompose()
#     for layer in ob.data.layers :
#         backup = src_ob.data.os_src_ob_data.list_matrix_transform.get(str(layer.info))
#         if backup is not None:
#             #backup transform data exists, use it
#             layer.location = backup.location
#             layer.rotation = backup.rotation
#             layer.scale = backup.scale
#         else:
#             layer.location = loc
#             layer.rotation = rot.to_euler()
#             layer.scale = sca

# def set_coordinates_world_mode(src_ob, ob, scene, cur_fr):
#     """Positions frames of given ghost according to position of source object on corresponding frames.

#     Args:
#         src_ob (Object): source object of ob (object used as reference)
#         ob (Object): ghost to update
#         scene (Scene): current scene
#         cur_fr (int): frame number of current frame / current instant on timeline
#     """
#     ob.animation_data_clear()
#     ob.parent = None
#     ob.location = [0,0,0]
#     ob.rotation_euler = [0,0,0]
#     ob.scale = [1,1,1]
#     for layer in ob.data.layers :
#         backup = src_ob.data.os_src_ob_data.list_matrix_transform.get(str(layer.info))
#         if backup is not None:
#             #backup transform data exists, use it
#             layer.location = backup.location
#             layer.rotation = backup.rotation
#             layer.scale = backup.scale
#         else:
#             scene.frame_set(int(layer.info))
#             loc, rot, sca = src_ob.matrix_world.decompose()
#             layer.location = loc
#             layer.rotation = rot.to_euler()
#             layer.scale = sca
#     scene.frame_set(cur_fr)

# def set_coordinates_camera_mode(src_ob, ob, scene, cur_fr):
#     """Positions frames of given ghost according to position of source object corresponding frames in relation to camera position.

#     Args:
#         src_ob (Object): source object of ob (object used as reference)
#         ob (Object): ghost to update
#         scene (Scene): current scene
#         cur_fr (int): frame number of current frame / current instant on timeline
#     """
#     if scene.camera is None:
#         set_coordinates_object_mode(src_ob, ob, scene, cur_fr)
#         return
#     ob.animation_data_clear()
#     ob.parent = None
#     ob.location = [0,0,0]
#     ob.rotation_euler = [0,0,0]
#     ob.scale = [1,1,1]
#     scene.frame_set(cur_fr)
#     cam_mat_current = scene.camera.matrix_world.copy() #save current world matrix of active camera
#     for layer in ob.data.layers :
#         backup = src_ob.data.os_src_ob_data.list_matrix_transform.get(str(layer.info))
#         if backup is not None:
#             #backup transform data exists, use it
#             layer.location = backup.location
#             layer.rotation = backup.rotation
#             layer.scale = backup.scale
#         else:
#             scene.frame_set(int(layer.info))
#             #get reference data
#             cam_mat_kf_invert = scene.camera.matrix_world.inverted()
#             old_glob = src_ob.matrix_world #old global data
#             local = cam_mat_kf_invert @ old_glob #local data in camera coordinate system, corresponding to old global
#             new_glob = cam_mat_current @ local #new global data : compute from local data according to current camera world_matrix
#             #set new calculated data
#             loc, rot, sca = new_glob.decompose()
#             layer.location = loc
#             layer.rotation = rot.to_euler()
#             layer.scale = sca
#     scene.frame_set(cur_fr)

def set_initial_frame_transform(src_ob, ob, scene, cur_fr, frame_nb):
    """Positions the given frame of given ghost at its initial position, according to current onion skin mode.

    Args:
        src_ob (Object): source object of ob (object used as reference)
        ob (Object): ghost to update
        scene (Scene): current scene
        cur_fr (int): frame number of current frame / current instant on timeline
        frame_nb (int): frame number of frame to position
    """
    #remove frame change handler
    while frame_handler in bpy.app.handlers.frame_change_post :
        bpy.app.handlers.frame_change_post.remove(frame_handler)
    try:
        os_ui_data = scene.osdata
        loc, rot, sca = None, None, None
        if os_ui_data.mode == 'ABS' :
            scene.frame_set(frame_nb)
            loc, rot, sca = src_ob.matrix_world.decompose()
            scene.frame_set(cur_fr)
        elif os_ui_data.mode == 'REL':
            scene.frame_set(cur_fr)
            loc, rot, sca = src_ob.matrix_world.decompose()
        elif os_ui_data.mode == 'CAM':
            scene.frame_set(cur_fr)
            if scene.camera is None:
                loc, rot, sca = src_ob.matrix_world.decompose()
            else:
                cam_mat_current = scene.camera.matrix_world.copy() #save current world matrix of active camera
                scene.frame_set(frame_nb)
                #get reference data
                cam_mat_kf_invert = scene.camera.matrix_world.inverted()
                old_glob = src_ob.matrix_world #old global data
                local = cam_mat_kf_invert @ old_glob #local data in camera coordinate system, corresponding to old global
                new_glob = cam_mat_current @ local #new global data : compute from local data according to current camera world_matrix
                #set new calculated data
                loc, rot, sca = new_glob.decompose()
                scene.frame_set(cur_fr)
        #set transforms
        layer = ob.data.layers[str(frame_nb)]
        layer.location = loc
        layer.rotation = rot.to_euler()
        layer.scale = sca
    finally:
        #add frame change handler
        bpy.app.handlers.frame_change_post.append(frame_handler)

'''--FRAME FUNCTIONS--'''
def count_sel_kf(gp_ob) :
    """Gets the list of selected frames frame_number attribute.

    Arg:
        gp_ob (GreasePencil): GreasePencil object where to look for selected frames

    Returns:
        list of int: list of frame numbers of selected frames
    """
    sel_fr_num = []
    for frame in gp_ob.layers[0].frames :
        if frame.select == True :
            sel_fr_num.append(frame.frame_number)
    return sel_fr_num

def get_kf_index(frame, layer):
    """Gets the index of a Gpencil frame, giving a frame.

    Args:
        frame (GPencilFrame): frame whose index is wanted
        layer (GPencilLayer): layer on which to look for the frame

    Returns:
        int: index of frame if found
        None: if frame not found
    """
    for i in range(len(layer.frames)) :
        if layer.frames[i] == frame :  
            return i
    return None

def get_kf_index_from_nb(frame_nb, layer):
    """Gets the index of a Gpencil frame, giving a frame number.

    Args:
        frame_nb (int): frame number whose index is wanted
        layer (GPencilLayer): layer on which to look for the frame

    Returns:
        int: index of frame if found
        None: if frame number not found
    """
    for i in range(len(layer.frames)) :
        if layer.frames[i].frame_number == frame_nb :
            return i
    return None

def get_fcurve_kf_index_from_nb(frame_nb, fcurve):
    """Gets the index of a fcurve keyframe, giving a frame number.

    Args:
        frame_nb (int): frame number whose index is wanted
        fcurve (FCurve): fcurve where to look for the keyframe

    Returns:
        int: index of keyframe if found
        None: if frame number not found
    """
    for i in range(len(fcurve.keyframe_points)):
        if fcurve.keyframe_points[i].co.x == frame_nb:
            return i
    return None

def get_gplyr_active_kf_number_at(layer, moment):
    """Gets the active frame number of a Gpencil layer for a given moment.
    Args:
        layer (GPencilLayer): layer on which to look for the active frame
        moment (int): timeline moment for which we search active frame

    Returns:
        int: frame number if an active frame has been found
        None: if no active frame found
    """
    nb = -1
    for frame in layer.frames:
        if nb < frame.frame_number <= moment:
            nb = frame.frame_number
    if nb == -1:
        return None
    return nb

def get_fcurve_active_kf_number_at(fcurve, moment):
    """Gets the keyframe number of the active fcurve keyframe for the given moment.

    Args:
        fcurve (FCurve): fcurve where to look for the active keyframe
        moment (int): timeline moment for which we search active keyframe

    Returns:
        int: keyframe number if an active keyframe has been found
        None: if no active keyframe found
    """
    nb = -1
    for keyframe_point in fcurve.keyframe_points:
        if nb < keyframe_point.co.x <= moment:
            nb = keyframe_point.co.x
    if nb == -1:
        return None
    return nb

def get_any_gp() :
    """Selects and gets any Gpencil object from the current scene.

    Returns:
        GreasePencil: if at least one Gpencil object has been found (random Gpencil object)
        None: if no Gpencil object found
    """
    gpencils = []
    for obj in bpy.context.scene.collection.all_objects :
        if obj.type == 'GPENCIL' :
            gpencils.append(obj)
    if not gpencils :
        return None
    gpencils[0].select_set(True)
    bpy.context.view_layer.objects.active = gpencils[0]
    return gpencils[0]

def get_surroundings_ghost_layers(ghost_gp, cur_fr, nb_frames_before, nb_frames_after,mode='ABSOLUTE'):
    '''Get the layers names of a ghost object which are surrounding the current frame.
    Args:
        ghost_gp (GreasePencil): ghost object
        cur_fr (int): current frame
        nb_frames_before (int): number of frames before current frame
        nb_frames_after (int): number of frames after current frame
        mode (str): onion skin mode
            'ABSOLUTE' : get layers of frames  directly surrounding the current frame. NONE if there isn't
            'RELATIVE' : get the closest layers surrounding the current frame, according to the current frame index. NONE if there isn't
    Returns:
        previous_layers (list of str): list of layers names of frames preceding the current frame
        next_layers (list of str): list of layers names of frames following the current frame
    '''
    previous_layers = []
    next_layers = []
    src_gplyr_name_list = get_gp_layername_list(ghost_gp)
    #convert to int and sort srclist
    src_gplyr_name_list = [int(i) for i in src_gplyr_name_list]
    src_gplyr_name_list.sort()
    if mode == 'ABSOLUTE':
        for i in range(max(nb_frames_before, nb_frames_after)):
            
                    
                previous_layer_name = next_layer_name = None  
                #get layers of frames directly surrounding the current frame            
                if i < nb_frames_before:
                    previous_layer_name = cur_fr - (i+1)
                    if previous_layer_name in src_gplyr_name_list:
                        previous_layers = [ previous_layer_name] + previous_layers
                    else:
                        previous_layers = [None]+ previous_layers
                if i < nb_frames_after:
                    next_layer_name = cur_fr + (i+1)
                    if next_layer_name in src_gplyr_name_list:
                        next_layers += [ next_layer_name]
                    else:
                        next_layers += [None]
    elif mode == 'RELATIVE':
        #get the closest layers surrounding the current frame in src_gplyr_name_list, according to the current frame index
        
        # iterate trrough the list get the closest inferior int  from cur_fr in src_gplyr_name_list
        pass

    return previous_layers, next_layers
            

def get_frames_number_before(src_ob, cur_fr, nb_frames_to_get):
    """Gets a list of some frames numbers preceding a given moment for a given object 
    (GPencil and Fcurve frames taken together).

    Args:
        src_ob (Object): considered object for frames numbers search
        cur_fr (int): frame number/timeline moment before which one frames numbers are wanted
        nb_frames_to_get (int): number of wanted frames preceding cur_fr

    Returns:
        list of int: list of at the most nb_frames_to_get frames numbers preceding cur_fr for src_ob.
    """
    before = set() #set to eliminate duplication
    active_gp_kf = []
    max_active_gp = -1
    for src_layer in src_ob.data.layers:
        if src_layer.use_onion_skinning is False:
            continue
        active_frame_nb = get_gplyr_active_kf_number_at(src_layer, cur_fr) #get active frame number for current frame
        if active_frame_nb is not None :
            index_before = get_kf_index_from_nb(active_frame_nb, src_layer) #get index of the active frame in the layer frames list
            active_gp_kf.append(src_layer.frames[index_before])
            if src_layer.frames[index_before].frame_number > max_active_gp:
                max_active_gp = src_layer.frames[index_before].frame_number
            index_before -= 1
            i = 0
            while(index_before >= 0 and i < nb_frames_to_get):
                #gets at the most nb_frames_to_get+1 numbers from this layer (+1 because we delete one frame later)
                if src_ob.data.os_src_ob_data.filter_kf_type == 'ALL' or src_ob.data.os_src_ob_data.filter_kf_type == src_layer.frames[index_before].keyframe_type:
                    before.add(src_layer.frames[index_before].frame_number)
                    i += 1
                index_before -= 1
    #fcurve keyframes:
    action_kf = set()
    active_fcurve_kf = []
    max_active_fcurve = -1
    if src_ob.animation_data is not None and src_ob.animation_data.action is not None:
        for fcurve in src_ob.animation_data.action.fcurves:
            active_kf_nb = get_fcurve_active_kf_number_at(fcurve, cur_fr)
            if active_kf_nb is not None:
                index_before = get_fcurve_kf_index_from_nb(active_kf_nb, fcurve)
                active_fcurve_kf.append(fcurve.keyframe_points[index_before])
                if fcurve.keyframe_points[index_before].co.x > max_active_fcurve:
                    max_active_fcurve = fcurve.keyframe_points[index_before].co.x
                index_before -= 1
                i = 0
                while(index_before >= 0 and i < nb_frames_to_get):
                    #gets at the most nb_frames_to_get+1 numbers from this fcurve (+1 because we delete one frame later)
                    if src_ob.data.os_src_ob_data.filter_kf_type == 'ALL' or src_ob.data.os_src_ob_data.filter_kf_type == fcurve.keyframe_points[index_before].type:
                        action_kf.add(int(fcurve.keyframe_points[index_before].co.x))
                        i += 1
                    index_before -= 1
    #find biggest number:
    nb = max(max_active_gp, max_active_fcurve)
    #complete kf lists with "false" active numbers:
    for fr in active_gp_kf:
        if fr.frame_number != nb and (src_ob.data.os_src_ob_data.filter_kf_type == 'ALL' or fr.keyframe_type == src_ob.data.os_src_ob_data.filter_kf_type):
            before.add(fr.frame_number)
    for fr in active_fcurve_kf:
        if fr.co.x != nb and (src_ob.data.os_src_ob_data.filter_kf_type == 'ALL' or fr.type == src_ob.data.os_src_ob_data.filter_kf_type):
            action_kf.add(fr.co.x)

    displayed_frames_before = list(before.union(action_kf))
    if displayed_frames_before:
        #sort frames numbers in ascending order
        displayed_frames_before.sort()
        if len(displayed_frames_before) > nb_frames_to_get:
            #keeps only the nb_frames_to_get nearest frames number if there is more in the list
            displayed_frames_before = displayed_frames_before[len(displayed_frames_before)-nb_frames_to_get:]
    return displayed_frames_before

def get_frames_number_after(src_ob, cur_fr, nb_frames_to_get):
    """Gets a list of some frames numbers following a given moment for a given object (GPencil and Fcurve frames taken together).

    Args:
        src_ob (Object): considered object for frames numbers search
        cur_fr (int): frame number/timeline moment after which one frames numbers are wanted
        nb_frames_to_get (int): number of wanted frames following cur_fr

    Returns:
        list of int: list of at the most nb_frames_to_get frames numbers following cur_fr for src_ob.
    """
    #GP keyframes:
    after = set() #set to eliminate duplication
    for src_layer in src_ob.data.layers:
        if src_layer.use_onion_skinning is False:
            #frames on layer which doesn't use onion skin are useless, skip the layer
            continue
        active_frame_nb = get_gplyr_active_kf_number_at(src_layer, cur_fr) #get active frame number for current frame
        if active_frame_nb is not None :
            index_active = get_kf_index_from_nb(active_frame_nb, src_layer) #get index of the active frame in the layer frames list
            index_after = index_active + 1 #first frame after current is surely the one following the active frame
        elif len(src_layer.frames) > 0:
            index_after = 0
        i = 0
        while(index_after < len(src_layer.frames) and i < nb_frames_to_get):
            #gets at the most nb_frames_to_get numbers from this layer
            if src_ob.data.os_src_ob_data.filter_kf_type == 'ALL' or src_ob.data.os_src_ob_data.filter_kf_type == src_layer.frames[index_after].keyframe_type:
                after.add(src_layer.frames[index_after].frame_number)
                i += 1
            index_after += 1
    #fcurve keyframes:
    action_kf = set()
    if src_ob.animation_data is not None and src_ob.animation_data.action is not None:
        for fcurve in src_ob.animation_data.action.fcurves:
            active_kf_nb = get_fcurve_active_kf_number_at(fcurve, cur_fr)
            if active_kf_nb is not None:
                index_active = get_fcurve_kf_index_from_nb(active_kf_nb, fcurve)
                index_after = index_active + 1
            elif len(fcurve.keyframe_points) > 0:
                index_after = 0
            i = 0
            while(index_after < len(fcurve.keyframe_points) and i < nb_frames_to_get):
                #gets at the most nb_frames_to_get numbers from this fcurve
                if src_ob.data.os_src_ob_data.filter_kf_type == 'ALL' or src_ob.data.os_src_ob_data.filter_kf_type == fcurve.keyframe_points[index_after].type:
                    action_kf.add(int(fcurve.keyframe_points[index_after].co.x))
                    i += 1
                index_after += 1

    displayed_frames_after = list(after.union(action_kf))
    if displayed_frames_after:
        #sort frames numbers in ascending order
        displayed_frames_after.sort()
        if len(displayed_frames_after) > nb_frames_to_get:
            #keeps only the nb_frames_to_get nearest frames number if there is more in the list
            displayed_frames_after = displayed_frames_after[:nb_frames_to_get]
    return displayed_frames_after

def create_src_frame_as_ghost_layer(src_obj,ghost_obj,frames_index_lists=[0,1],
                                    stroke_marker = 'STROKE',
                                    fill_marker='FILL',
                                    current_frame = 0,
                                    verbose = False):
    '''Creates a layer per frame of source object, and copy all the frame layers stroke on the layer.'''

    start_time = time.time()
    for frame_nb in frames_index_lists:
        tgt_layer=None
        #iterate trough all layers of source object
        for src_layer in src_obj.data.layers:
            is_fill_layer=is_stroke_layer=False
            if stroke_marker in src_layer.info:
                is_stroke_layer = True
            elif fill_marker in src_layer.info:
                is_fill_layer = True


            if (src_layer.use_onion_skinning is False or 
                (not is_stroke_layer and not is_fill_layer)):
                #skip this layer
                continue
            #get active keyframe for kf frame number
            active_nb = get_gplyr_active_kf_number_at(src_layer, frame_nb)
            if active_nb == frame_nb:
                active_fr = get_now_gpframe(active_nb, src_layer)
                need_update = True
                # need_update = has_hash_change(active_fr)
                
                if tgt_layer is None:
                    tgt_layer = get_gplyr_from_name(ghost_obj, str(frame_nb))
                    if tgt_layer is None:
                        if verbose: print('create layer', frame_nb)
                        tgt_layer = ghost_obj.data.layers.new(str(frame_nb))
                        #create a new layer to represents the frame
                    else:
                        if verbose: print('layer', frame_nb, 'already exists')
                        # TODO check if necessary
                        tgt_layer.clear()

                
                
                if active_fr is not None and need_update:
                    # tgt_layer.clear()
                    #get first frame of the layer if len>0
                    if len(tgt_layer.frames)>0:
                        tgt_frame = tgt_layer.frames[0]
                        #clear frame
                        # tgt_frame.clear()
                    else:
                        tgt_frame = tgt_layer.frames.new(0)
                    
                    scene = bpy.context.scene
                    os_ui_data = scene.osdata
                    if frame_nb < current_frame:
                        if is_stroke_layer:
                            material_name = os_ui_data.before_material_name
                        else:
                            material_name = os_ui_data.before_fill_material_name
                    else:
                        if is_stroke_layer:
                            material_name = os_ui_data.after_material_name
                        else:
                            material_name = os_ui_data.after_fill_material_name
                    
                    i=0
                    for mats in ghost_obj.data.materials:
                        if mats.name == material_name:
                            new_material_index = i 
                            break
                        i+=1
                    
                    #copy the strokes of the active keyframe on the layer
                    if need_update:
                        for src_stroke in active_fr.strokes:
                            
                            tgt_stroke = tgt_frame.strokes.new()
                            stroke_confo(src_stroke, 
                                         tgt_stroke,
                                         points_attributes=['co','pressure','strength'],
                                         )
                            #time control
                            if verbose: 
                                print('stroke conformed in', time.time() - start_time)
                                start_time = time.time()
                            #apply appropriate mat
                            tgt_stroke.material_index = new_material_index
                    else:
                        for tgt_stroke in tgt_frame.strokes:
                            tgt_stroke.material_index = new_material_index
        
#TO GET RID OFF
# def complete_with_frames_before_after(src_ob, tgt_ob, frames_numbers):
#     """Completes a given object, copying frames from another object.

#     Args:
#         src_ob (Object): object from which frames are copied
#         Tgt_ob (Object): object to which frames are copied
#         frames_numbers (list of int): list of frames numbers of all frames we want to copy
#     """
#     for src_layer in src_ob.data.layers:
#         if src_layer.use_onion_skinning is False:
#             #skip this layer
#             continue
#         layer = tgt_ob.data.layers.new(src_layer.info)
#         for frame_nb in frames_numbers:
#             #for all frames wanted, copy the frame or create a blank one on the new layer of ob
#             #get active keyframe for kf frame number
#             active_nb = get_gplyr_active_kf_number_at(src_layer, frame_nb)
#             new_fr = None
#             if active_nb :
#                 active_fr = get_now_gpframe(active_nb, src_layer)
#                 new_fr = layer.frames.copy(active_fr) #copy active keyframe
#                 new_fr.frame_number = frame_nb
#             else :
#                 #no active keyframe : create blank keyframe
#                 new_fr = layer.frames.new(frame_nb)
#             #set keyframe types
#             if src_ob.data.os_src_ob_data.filter_kf_type != 'ALL':
#                 new_fr.keyframe_type = src_ob.data.os_src_ob_data.filter_kf_type

# def create_layer_per_frame(src_ob, ob, cur_fr, put_blank_kf_on_current_frame):
#     """Completes a given object, creating a layer per frame of source object.

#     Args:
#         src_ob (Object): object from which frames are identified
#         ob (Object): object on which layers per frame are created (give same object for src_ob and ob to create layer per frame directly on source object)
#         cur_fr (int): frame number of current frame / current instant on timeline
#         put_blank_kf_on_current_frame (bool): if True adds a blank keyframe at cur_fr instant

#     Side effect:
#         Merge all layers of src_ob, if different from ob it needs to be an object that can be modified without causing problems
#     """
#     if src_ob is None or src_ob.data is None or len(src_ob.data.layers) < 1 or ob is None or ob.data is None:
#         return
#     frames_nb = []
#     active_ob = bpy.context.view_layer.objects.active
#     bpy.context.view_layer.objects.active = src_ob
#     #merge all layers into one
#     for i in range(len(src_ob.data.layers)-1):
#         src_ob.data.layers.active = src_ob.data.layers[-1]
#         bpy.ops.gpencil.layer_merge()#TODO remove operator
#     src_ob.data.layers[0].info =  ghost_prefix + src_ob.data.layers[0].info #rename to prevent potential .001 added
#     if put_blank_kf_on_current_frame:
#         #add a blank keyframe on the current frame
#         src_ob.data.layers[0].frames.new(cur_fr)
#     refresh_dopesheets()

#     frames = src_ob.data.layers[0].frames
#     if len(frames) > 0:
#         for i in range(len(frames)):
#             frames_nb.append(frames[i].frame_number)
#             #create a new layer to represents the frame
#             layer = ob.data.layers.new(str(frames[i].frame_number)) #its name is the frame number
#             layer.frames.copy(frames[i])
#         src_ob.data.layers.remove(src_ob.data.layers[0]) #remove the previously merged layer from src_ob
#         #complete each layer with blank keyframe on each frame not represented by the layer
#         for layer in ob.data.layers:
#             for frame_nb in frames_nb:
#                 if int(layer.info) != frame_nb:
#                     layer.frames.new(frame_nb)
#     bpy.context.view_layer.objects.active = active_ob #restore previous active object


'''DOPE SHEET FUNCTIONS'''
def refresh_dopesheets():
    """Refreshes Gpencil and Dopesheet modes of dopesheet editor."""
    # print('::::::::::::dopesheets refresh :::::::::')
    if bpy.context.object is None or bpy.context.object.type != 'GPENCIL':
        gp = get_any_gp()
        if gp is None and len(bpy.context.scene.collection.all_objects) > 0:
            bpy.context.view_layer.objects.active = bpy.context.scene.collection.all_objects[0]
    refresh_dopesheet_mode('GPENCIL')
    refresh_dopesheet_mode('DOPESHEET')

def refresh_dopesheet_mode(mode) :
    """Forces blender to refresh frames indices in a specific mode of dopesheet.

    Arg:
        mode (str): mode of dopesheet editor to refresh
    """
    if bpy.context.object is None or (mode == 'GPENCIL' and bpy.context.object.type != 'GPENCIL'):
        return
    if bpy.context.area :
        area = bpy.context.area
    elif len(bpy.context.screen.areas) > 0 :
        area = bpy.context.screen.areas[0]
    cur_areatype = str(area.type)
    area.type = 'DOPESHEET_EDITOR'
    cur_space_mode = str(area.spaces[0].mode)
    area.spaces[0].mode = mode
    #create a customized context to avoid problem of missing area in bpy.context
    customized_context = bpy.context.copy()
    customized_context['area'] = area
    #call the operators with the customized context
    bpy.ops.action.mirror(customized_context, type = 'XAXIS')
    bpy.ops.action.mirror(customized_context, type = 'XAXIS')
    area.spaces[0].mode = cur_space_mode
    area.type = cur_areatype
    # print('refreshed '+mode+' of dopesheet editor')


'''--MISC FUNCTIONS--'''

def is_native_os_enabled():
    """determine whether the native onion_skin is enabled or not"""
    result = False
    # depending on context area may not be avail.
    if bpy.context.area is not None:
        return bpy.context.area.spaces[0].overlay.use_gpencil_onion_skin
    
    for area in bpy.context.screen.areas:
        try:
            result = area.spaces[0].overlay.use_gpencil_onion_skin
            return result        
        except:
            pass
    return result


#REPLACED
# def get_kf(frame_nb, layer):
#     """Gets a Gpencil frame, giving a frame_number

#     Args:
#         frame_nb (int): frame number of wanted frame
#         layer (GPencilLayer): layer on which to look for the frame

#     Returns:
#         GPencilFrame: frame if found
#         None: if frame number not found
#     """
#     for fr in layer.frames:
#         if fr.frame_number == frame_nb:
#             return fr
#     return None

def set_parent_name(parent):
    parent.os_src_ob_data.parent_name = parent.name