from .common.gp_utils import get_material_by_name
from .common.utils import register_classes, unregister_classes
import bpy
from .core import get_ghost, ghost_opacity_gradient, set_initial_frame_transform, set_coordinates, update_ghost_collection, clear_obj_ghost, op_generate, update_filter_kf_type, frame_handler, pre_render, post_render,scene_update_post
from bpy_extras import view3d_utils
from bpy.props import (StringProperty,
                       PointerProperty,
                       )
from bpy.types import (Panel,
                       Operator,
                       AddonPreferences,
                       PropertyGroup,
                       )
from math import pi, radians
from mathutils import Vector, Matrix
from bpy.props import *
import bgl
import blf
from gpu import shader
from gpu_extras.batch import batch_for_shader
import subprocess

#----------------------------------------#                            
######   MOVE KF MODAL OPERATOR    #####  
#----------------------------------------#                                   
class OS_OT_MOVE_KF(bpy.types.Operator):     
    bl_label = "Move Onion skin frame"
    bl_idname = "os.move"
    bl_options = {'REGISTER','UNDO'}
    fr_num : bpy.props.IntProperty() 
    current_ghost_name : bpy.props.StringProperty()
    
    mode = None
    draw_handler = None

    ghosts=[]
    ghost_layers = []
    invoke_locations = []
    invoke_rotations = []
    invoke_scales = []

    mat_loc_array = []
    mat_loc2_array = []
    
    rot_angle_array = []
    mat_rot_array = []
    mat_rot2_array = []
    
    mat_sca_array = []
    mat_sca2_array = []



    def __init__(self):
        print("Start")

    def __del__(self):
        print("End")

    def grab(self, context):
        """Translates onion skin instance."""
        #Contextual active object, 2D and 3D regions
        scene = bpy.context.scene
        region = bpy.context.region
        region3D = bpy.context.space_data.region_3d

        #The direction indicated by the mouse position from the current view
        self.view_vector = view3d_utils.region_2d_to_vector_3d(region, region3D, self.mouse_pos)
        #The 3D location in this direction
        self.world_loc = view3d_utils.region_2d_to_location_3d(region, region3D, self.mouse_pos, self.view_vector)
        self.relat_loc = Vector(self.world_loc - view3d_utils.region_2d_to_location_3d(region, region3D, self.init_loc, self.view_vector))
        
        #apply transformations
        for i,ghost_layer in enumerate(self.ghost_layers):
            self.mat_loc_array[i] = Matrix.Translation(self.relat_loc)
            #define the final transformation matrix (take previous iteration back and add the new one)
            transform_mat = self.mat_loc_array[i] @ self.mat_loc2_array[i] @ ghost_layer.matrix_layer
            #store the transfomation inverse for the next iteration
            mat_loc2 = self.mat_loc_array[i]
            mat_loc2.invert()
            self.mat_loc2_array[i] = mat_loc2
            #update layer location
            loc, rot, sca = transform_mat.decompose()
            ghost_layer.location = loc
            #update pivot point for draw handler
        self.pivot = [self.ghost_layers[0].location.to_tuple()]
        self.batch = batch_for_shader(self.shader, 'POINTS', {'pos': self.pivot})
        return {'FINISHED'}
     
    def rotate(self, context): 
        """Rotates onion skin instance."""
        #get context
        region = bpy.context.region
        region3D = bpy.context.space_data.region_3d

        #calculate rotation pivot and get view orientation
        pivot = self.ghost_layers[0].location
        pivot2d = view3d_utils.location_3d_to_region_2d(region, region3D, pivot, default = None)
        origin2d = view3d_utils.location_3d_to_region_2d(region, region3D, pivot, default = None)
        orientation = view3d_utils.region_2d_to_vector_3d(region, region3D, origin2d)
        rot_angle = (Vector(self.mouse_pos)-pivot2d).angle_signed(self.init_loc - pivot2d)
        mat_rot = Matrix.Rotation(-rot_angle, 4, orientation)
        #get mouse rotation input
        for i,ghost_layer in enumerate(self.ghost_layers):
            #calculate rotation pivot and get view orientation
            pivot = ghost_layer.location
            pivot2d = view3d_utils.location_3d_to_region_2d(region, region3D, pivot, default = None)
            origin2d = view3d_utils.location_3d_to_region_2d(region, region3D, pivot, default = None)
            orientation = view3d_utils.region_2d_to_vector_3d(region, region3D, origin2d)
            rot_angle = (Vector(self.mouse_pos)-pivot2d).angle_signed(self.init_loc - pivot2d)
            mat_rot = Matrix.Rotation(-rot_angle, 4, orientation)
            self.rot_angle_array[i] = rot_angle
            #define the rotation matrix
            self.mat_rot_array[i] = mat_rot
            #define the final transformation matrix (take previous iteration back and add the new one)
            transform_mat = self.mat_rot_array[i] @ self.mat_rot2_array[i] @ ghost_layer.matrix_layer
            #store the transfomation inverse for the next iteration
            mat_rot2 = mat_rot
            mat_rot2.invert()
            self.mat_rot2_array[i] = mat_rot2
            #update layer rotation
            loc, rot, sca = transform_mat.decompose()
            ghost_layer.rotation = rot.to_euler()
        return {'FINISHED'}       
    
    def scale(self, context): 
        """Scales onion skin instance."""
        #get context
        region = bpy.context.region
        region3D = bpy.context.space_data.region_3d
        
        #get pivot point and calculate the scale mouse input
        pivot2d = view3d_utils.location_3d_to_region_2d(region, region3D, self.ghost_layers[0].location, default = None)
        a = Vector(self.mouse_pos) - pivot2d
        b = self.init_loc - pivot2d
        scale_factor = a.length/b.length
        if abs(b.angle_signed(a)) > radians(90):
            scale_factor = -scale_factor
        #define the scale matrix
        for i,ghost_layer in enumerate(self.ghost_layers):
            self.mat_sca_array[i] = Matrix.Scale(scale_factor, 4)
            #define the final transformation matrix (take previous iteration back and add the new one)
            transform_mat = self.mat_sca_array[i] @ self.mat_sca2_array[i] @ ghost_layer.matrix_layer
            #store the transfomation inverse for the next iteration
            mat_sca2 = self.mat_sca_array[i]
            mat_sca2.invert() 
            self.mat_sca2_array[i] = mat_sca2
            #update layer scale
            loc, rot, sca = transform_mat.decompose()
            ghost_layer.scale = sca
        return {'FINISHED'}
    
    def reset(self,context):
        """Resets ghost layer to initial state."""
        for ghost_layer,loc,rot,sca in zip(self.ghost_layers,self.invoke_locations,self.invoke_rotations,self.invoke_scales):
            ghost_layer.location = loc
            ghost_layer.rotation = rot
            ghost_layer.scale = sca
        # self.ghost_layer.location = self.invoke_location
        # self.ghost_layer.rotation = self.invoke_rotation
        # self.ghost_layer.scale = self.invoke_scale
        return {'FINISHED'} 

    def draw_controls(self):
        """Draws list of available controls for transformation mode."""
        font_id = 0
        blf.color(font_id, 1.0, 0.36, 0.36, 1.0)
        blf.size(font_id, 15, 72)
        blf.position(font_id, 10, 100, 0)
        blf.draw(font_id, "TRANSFORMATION MODE")
        blf.color(font_id, 1.0, 0.97, 0.93, 1.0)
        blf.size(font_id, 15, 72)
        blf.position(font_id, 10, 80, 0)
        blf.draw(font_id, "Controls:")
        blf.size(font_id, 12, 72)
        blf.position(font_id, 10, 60, 0)
        blf.draw(font_id, "Move: G")
        blf.position(font_id, 10, 45, 0)
        blf.draw(font_id, "Rotate: R")
        blf.position(font_id, 10, 30, 0)
        blf.draw(font_id, "Scale: S")

    def draw_pivot(self):
        """Draws point(s) on viewport, used to draw pivot point."""
        self.shader.bind()
        self.shader.uniform_float("color", (1.0, 0.79, 0.0, 1.0))
        self.batch.draw(self.shader)
       
    def modal(self, context, event):  
        if event.type == 'MOUSEMOVE':         
            self.mouse_pos = event.mouse_region_x, event.mouse_region_y
            #get the mouse position and run the transfomation methods
            if self.mode == 'GRAB':
                self.grab(context)
            if self.mode == 'ROTATE':
                self.rotate(context)   
            if self.mode == 'SCALE':
                self.scale(context)

        elif event.type == 'LEFTMOUSE':  # Confirm
            #remove draw handlers
            bpy.types.SpaceView3D.draw_handler_remove(self.draw_handler_controls, 'WINDOW')
            bpy.types.SpaceView3D.draw_handler_remove(self.draw_handler_pivot, 'WINDOW')
            bpy.context.view_layer.objects.active = bpy.context.object #to force callback to be erased immediately
            #update backup transform info for this frame
            for ghost,ghost_layer in zip(self.ghosts,self.ghost_layers):
                backup = ghost.data.os_src_ob_data.list_matrix_transform.get(str(self.fr_num))
                if backup is None:
                    #no existing backup, create a new one for this frame number
                    backup = ghost.data.os_src_ob_data.list_matrix_transform.add()
                    backup.name = str(self.fr_num)
                loc, rot, sca = ghost_layer.matrix_layer.decompose()
                backup.location = loc
                backup.rotation = rot.to_euler()
                backup.scale = sca
            return {'FINISHED'}

        #switch transfomation mode
        elif event.type == 'G':  
            self.mode = 'GRAB'
            self.init_loc = Vector(self.mouse_pos)
            return {'RUNNING_MODAL'}
        
        elif event.type == 'R': 
            self.mode = 'ROTATE'
            self.init_loc = Vector(self.mouse_pos)
            return {'RUNNING_MODAL'}
        
        elif event.type == 'S':  
            self.mode = 'SCALE'
            self.init_loc = Vector(self.mouse_pos)
            return {'RUNNING_MODAL'}
        
        elif event.type in {'RIGHTMOUSE', 'ESC'}:  # Cancel
            self.reset(context)
            #remove draw handlers
            bpy.types.SpaceView3D.draw_handler_remove(self.draw_handler_controls, 'WINDOW')
            bpy.types.SpaceView3D.draw_handler_remove(self.draw_handler_pivot, 'WINDOW')
            bpy.context.view_layer.objects.active = bpy.context.object #to force callback to be erased immediately
            return {'CANCELLED'}    

        return {'PASS_THROUGH'}

    def invoke(self, context, event):
        if context.area.type == 'VIEW_3D':
            #clear all array
            self.ghosts.clear()
            self.ghost_layers.clear()
            self.invoke_locations.clear()
            self.invoke_rotations.clear()
            self.invoke_scales.clear()
            self.mat_loc_array.clear()
            self.mat_loc2_array.clear()
            self.rot_angle_array.clear()
            self.mat_rot_array.clear()
            self.mat_rot2_array.clear()
            self.mat_sca_array.clear()
            self.mat_sca2_array.clear()

            #get context
            self.mouse_pos = event.mouse_region_x, event.mouse_region_y
            region = bpy.context.region
            region3D = bpy.context.space_data.region_3d
            self.init_loc = Vector(self.mouse_pos)
            
            #get inital state
            self.view_vector = view3d_utils.region_2d_to_vector_3d(region, region3D, self.mouse_pos)
            self.world_loc = view3d_utils.region_2d_to_location_3d(region, region3D, self.mouse_pos, self.view_vector)
            # src_ob = bpy.context.object
            layers_names = self.current_ghost_name.split(',')

            for i,layer_name in enumerate(layers_names):
                ghost = bpy.data.objects.get(layer_name)
                if ghost is None:
                    continue     
                ghost_layer = ghost.data.layers.get(str(self.fr_num))     
                if ghost_layer is None:
                    continue

                # self.ghost = bpy.data.objects.get(self.current_ghost_name)
                self.ghosts.append( ghost)
                # ghost_layer = ghost.data.layers[str(self.fr_num)]
                self.ghost_layers.append( ghost_layer)
                #get initial transformation values
                self.invoke_locations.append( ghost_layer.location.copy())
                self.invoke_rotations.append( ghost_layer.rotation.copy())
                self.invoke_scales.append( ghost_layer.scale.copy())
                #create transformation matrixes
                self.mat_loc_array.append( Matrix())
                self.mat_loc2_array.append( Matrix())                
                self.rot_angle_array.append( 0.0)
                self.mat_rot_array.append( Matrix())
                self.mat_rot2_array.append( Matrix())                
                self.mat_sca_array.append( Matrix())
                self.mat_sca2_array.append( Matrix())

            #tools to draw pivot point on viewport
            self.pivot = [self.ghost_layers[0].location.to_tuple()]
            self.shader = shader.from_builtin('3D_UNIFORM_COLOR')
            self.batch = batch_for_shader(self.shader, 'POINTS', {'pos': self.pivot})
            #add draw handlers
            self.draw_handler_controls = bpy.types.SpaceView3D.draw_handler_add(self.draw_controls, (), 'WINDOW', 'POST_PIXEL')
            self.draw_handler_pivot = bpy.types.SpaceView3D.draw_handler_add(self.draw_pivot, (), 'WINDOW', 'POST_VIEW')
            bpy.context.view_layer.objects.active = bpy.context.object #to force callback to be drawn immediately

            context.window_manager.modal_handler_add(self)
            return {'RUNNING_MODAL'}
        else:
            self.report({'WARNING'}, "View3D not found, cannot run operator")
            return {'CANCELLED'}

class OS_OT_DELETE_FRAME_TRANSFORM(bpy.types.Operator):
    """Reset ghost frame"""
    bl_label = "Delete frame transformation"
    bl_idname = "os.delete_frame_transform"
    bl_options = {'UNDO'}
    fr_num : bpy.props.IntProperty()

    def execute(self, context):
        src_ob = context.object
        backup_index = src_ob.data.os_src_ob_data.list_matrix_transform.find(str(self.fr_num))
        if backup_index != -1:
            #backup for this frame exists, remove it
            src_ob.data.os_src_ob_data.list_matrix_transform.remove(backup_index)
            ghost = get_ghost(src_ob)
            if ghost is not None:
                #reinitiate ghost transform for this frame
                set_initial_frame_transform(src_ob, ghost, context.scene, context.scene.frame_current, self.fr_num)
        return {'FINISHED'}

class OS_OT_DELETE_TRANSFORMS(bpy.types.Operator):
    """Reset all ghost position, scale, ..."""
    bl_label = "Delete all ghost transformations"
    bl_idname = "os.delete_transforms"
    bl_options = {'UNDO'}
    def execute(self, context):
        src_ob = context.object
        #delete backup for all frames of object
        src_ob.data.os_src_ob_data.list_matrix_transform.clear()
        ghost = get_ghost(src_ob)
        if ghost is not None:
            #reinitiate ghost transforms for all frames
            set_coordinates(src_ob, ghost, context.scene, context.scene.frame_current)
        return {'FINISHED'}

#####OTHER#####
class OS_OT_CLEAR(bpy.types.Operator):
    """Disable plugin features"""
    bl_label = "Go back to default onion skinning"
    bl_idname = 'os.clear'
    bl_options = {'REGISTER','UNDO'}   
    def execute (self, context) :
        
        # bpy.context.scene.osdata.mode = 'DIS'

        bpy.context.scene.osdata.semaphore = True
        OScollection = bpy.data.collections.get("ghost_collection")
        if OScollection is not None:
            #remove all object in collection
            ghosts =  OScollection.all_objects
            for ghost in ghosts:
                ghost_data = ghost.data
                bpy.data.objects.remove(ghost)
                bpy.data.grease_pencils.remove(ghost_data)
               
            #remove the collection
            bpy.data.collections.remove(OScollection)
        bpy.context.scene.osdata.semaphore = False
        return {'FINISHED'}
    
# class OS_OT_GENERATE(bpy.types.Operator):
#     """set World as reference frame"""
#     bl_label = "Generate Onion Skinning based on mode"
#     bl_idname = 'os.generate'
#     # def mode_update(self, context):
#     #     op_generate(context,self.generate_mode)

#     generate_mode: bpy.props.StringProperty(default = 'REL', 
#                                             # update =  mode_update
#                                             )
#     bl_options = {'REGISTER','UNDO'}
#     def execute (self, context) :
#         op_generate(context,self.generate_mode)
#         return {'FINISHED'}

# class OS_OT_GENERATE_ABS(bpy.types.Operator):
#     """set World as reference frame"""
#     bl_label = "Generate Absolute Onion Skinning"
#     bl_idname = 'os.generate_abs'
#     bl_options = {'REGISTER','UNDO'}

#     def execute (self, context) :
#         op_generate(context, 'ABS')
#         return {'FINISHED'}
    
# class OS_OT_GENERATE_LOC(bpy.types.Operator):
#     """set Object as reference frame"""
#     bl_label = "Generate Relative Onion Skinning"
#     bl_idname = 'os.generate_loc'
#     bl_options = {'REGISTER','UNDO'}
#     def execute (self, context) :
#         op_generate(context, 'REL')
#         return {'FINISHED'}

# class OS_OT_GENERATE_CAM(bpy.types.Operator):
#     """set Camera as reference frame"""
#     bl_label = "Generate Camera Relative Onion Skinning"
#     bl_idname = 'os.generate_cam'
#     bl_options = {'REGISTER','UNDO'}
#     def execute (self, context) :
#         op_generate(context, 'CAM')
#         return {'FINISHED'}     
    
# class OS_OT_USE_OS(bpy.types.Operator):
#     """Enable/Disable native onion skin"""
#     bl_label = "Enable/Disable onion skin"
#     bl_idname = 'os.use_onion_skin'

#     def execute (self, context) :
#         #get 3D view spaces
#         space3D = []
#         for area in context.screen.areas :
#             if area.type == "VIEW_3D":
#                 #spaces[0] is current space
#                 space3D.append(area.spaces[0])
#                 break
#         if not space3D:
#             return{'FINISHED'}

#         #update data
#         for space in space3D:
#             # toggle native onion_skin
#             space.overlay.use_gpencil_onion_skin = not space.overlay.use_gpencil_onion_skin #invert previous value for addon OS activation        
        
#         OScollection = bpy.data.collections.get("ghost_collection")
#         if OScollection is not None :
#             OScollection.hide_viewport = not space.overlay.use_gpencil_onion_skin #hide ghost collection or not
#             if space.overlay.use_gpencil_onion_skin :
#                 update_ghost_collection()
#             else:
#                 #restore in_front value of src objects
#                 for ghost in OScollection.all_objects:
#                     src_ob = bpy.data.objects.get(ghost.name[6:])
#                     if src_ob is not None:
#                         src_ob.show_in_front = src_ob.data.os_src_ob_data.src_in_front_value
         

#         return {'FINISHED'}

# class OS_OT_RESET_OPACITIES(bpy.types.Operator):
#     """Reset the opacity values of a ghost"""
#     bl_label = "Reset opacities"
#     bl_idname = 'os.reset_opacities'
#     bl_options = {'REGISTER','UNDO'}

#     def execute(self, context):
#         src_ob = context.object
#         if src_ob is not None:
#             ghost = get_ghost(src_ob)
#             if ghost is not None:
#                 ghost.data.os_current_ghost_opacities = ghost_opacity_gradient
#         return {'FINISHED'}

#----------------------------------------#                            
######        DATA HOLDERS           #####  
#----------------------------------------#  

# def update_ghost_before_range(self, context):
#     """Updates ghost object according to new ghost_before_range value."""
#     # offset = self.ghost_before_range - self.previous_ghost_before_range #compute difference between old and new value
#     # src_ob = context.object
#     # ghost = get_ghost(src_ob)
#     # if ghost is not None:
#     #     if is_native_os_enabled() is True:
#     #         if offset > 0:
#     #             #add new frames
#     #             if len(ghost.data.os_ghost_data["displayed_frames_before"]) < 1:
#     #                 #no preceding frame displayed before, we want frames preceding the current frame
#     #                 farthest_frame_nb = context.scene.frame_current
#     #             else:
#     #                 #some preceding frames have already been displayed, we want frames preceding the farthest displayed frame before current
#     #                 farthest_frame_nb = ghost.data.os_ghost_data["displayed_frames_before"][0]
#     #             add_frames_to_ghost(src_ob, ghost, "before", farthest_frame_nb, offset, context) #add new preceding frames to ghost
#     #         elif offset < 0:
#     #             #need to remove frames
#     #             #update offset with the real number of frames to remove
#     #             #ghost_before_range value can be much bigger than numbers of frames displayed if src object had no more frames before
#     #             offset = self.ghost_before_range - len(list(ghost.data.os_ghost_data["displayed_frames_before"]))
#     #             if offset < 0:
#     #                 frames_to_keep = list(ghost.data.os_ghost_data["displayed_frames_before"][-offset:])
#     #                 frames_to_remove = list(ghost.data.os_ghost_data["displayed_frames_before"][:-offset])
#     #                 ghost.data.os_ghost_data["displayed_frames_before"] = frames_to_keep
#     #                 remove_frames_from_ghost(ghost, frames_to_remove) #remove frames
#     #     ghost.data.ghost_before_range = self.ghost_before_range #update Blender ghost_before_range value of the ghost
#     self.previous_ghost_before_range = self.ghost_before_range 
#     update_ghost_collection()
#     #new value becomes previous value for next time

# def update_ghost_after_range(self, context):
#     """Updates ghost object according to new ghost_after_range value."""
#     # offset = self.ghost_after_range - self.previous_ghost_after_range #compute difference between old and new value
#     # src_ob = context.object
#     # ghost = get_ghost(src_ob)
#     # if ghost is not None:
#     #     if is_native_os_enabled() is True:
#     #         if offset > 0:
#     #             #add new frames
#     #             if len(ghost.data.os_ghost_data["displayed_frames_after"]) < 1:
#     #                 #no following frame displayed before, we want frames following the current frame
#     #                 farthest_frame_nb = context.scene.frame_current
#     #             else:
#     #                 #some following frames have already been displayed, we want frames following the farthest displayed frame after current
#     #                 farthest_frame_nb = ghost.data.os_ghost_data["displayed_frames_after"][-1]
#     #             add_frames_to_ghost(src_ob, ghost, "after", farthest_frame_nb, offset, context) #add new following frames to ghost
#     #         elif offset < 0:
#     #             #need to remove frames
#     #             #update offset with the real number of frames to remove
#     #             #ghost_after_range value can be much bigger than numbers of frames displayed if src object had no more frames after)
#     #             offset = self.ghost_after_range - len(list(ghost.data.os_ghost_data["displayed_frames_after"]))
#     #             if offset < 0:
#     #                 frames_to_keep = list(ghost.data.os_ghost_data["displayed_frames_after"][:offset])
#     #                 frames_to_remove = list(ghost.data.os_ghost_data["displayed_frames_after"][offset:])
#     #                 ghost.data.os_ghost_data["displayed_frames_after"] = frames_to_keep
#     #                 remove_frames_from_ghost(ghost, frames_to_remove) #remove frames
#         # ghost.data.ghost_after_range = self.ghost_after_range #update Blender ghost_after_range value of the ghost
#     update_ghost_collection
#     self.previous_ghost_after_range = self.ghost_after_range #new value becomes previous value for next time


def reverse_enabling(self, context):
            #get 3D view spaces
        space3D = []
        for area in context.screen.areas :
            if area.type == "VIEW_3D":
                #spaces[0] is current space
                space3D.append(area.spaces[0])
                break
        if not space3D:
            return{'FINISHED'}

        #update data
        for space in space3D:
            # toggle native onion_skin
            space.overlay.use_gpencil_onion_skin = not space.overlay.use_gpencil_onion_skin #invert previous value for addon OS activation        
        
        OScollection = bpy.data.collections.get("ghost_collection")
        if OScollection is not None :
            OScollection.hide_viewport = not space.overlay.use_gpencil_onion_skin #hide ghost collection or not
            if space.overlay.use_gpencil_onion_skin :
                update_ghost_collection()
            else:
                #restore in_front value of src objects
                for ghost in OScollection.all_objects:
                    src_ob = bpy.data.objects.get(ghost.name[6:])
                    if src_ob is not None:
                        src_ob.show_in_front = False

def on_settings_update(self, context):
    """Updates ghost object according to new ghost_before_range value."""
    if not self.semaphore:
        self.semaphore = True
    else:
        if self.mode == 'DIS':
            self.semaphore = False
        else:
            return
    
    if self.mode == 'DIS':
        if self.enabled is True:
            self.enabled = False            
        OScollection = bpy.data.collections.get("ghost_collection")
        if OScollection is not None:
            #Hide collection
            OScollection.hide_viewport = True
            # if bpy.context.scene.osdata.target == 'object':
            #     clear_obj_ghost(bpy.context.object)
            # elif bpy.context.scene.osdata.target == 'collection':
            #     listObj = bpy.context.collection.all_objects.values()
            #     for obj in listObj:
            #         clear_obj_ghost(obj)
    else:
        if self.enabled is False:
            self.enabled = True
        
        update_ghost_collection()
        # op_generate(context,self.mode)
    self.semaphore = False

class OS_DATA(PropertyGroup):
    bl_label = "GP Onionskin Properties"
    bl_idname = "osdata"
    
    # ghost_layers : bpy.props.CollectionProperty(type = bpy.types.GPencilLayer)
    semaphore : bpy.props.BoolProperty(default = False)
    refresh_on_select : bpy.props.BoolProperty(default = False)
    enabled : bpy.props.BoolProperty(default = False,update=reverse_enabling)

    cur_fr : bpy.props.IntProperty()
    force_in_front : bpy.props.BoolProperty(default = False,
                                             update = on_settings_update)
    ghost_in_front : bpy.props.BoolProperty(default = False,
                                                update = on_settings_update)
    src_onion_factor : bpy.props.FloatProperty(default = 0.0)
    ghost_before_range : bpy.props.IntProperty(default = 2, min = 0, soft_min = 0, max=32,
                                               update = on_settings_update#update_ghost_before_range
                                               )
    ghost_after_range : bpy.props.IntProperty(default = 2, min = 0, soft_min = 0, max=32,
                                              update = on_settings_update#update_ghost_after_range
                                              )
    src_ghost_before_range : bpy.props.IntProperty(default = 0)
    src_ghost_after_range : bpy.props.IntProperty(default = 0)

    displayed_frames_before : bpy.props.IntVectorProperty(name = "displayed_frames_before", size = 10)
    displayed_frames_after : bpy.props.IntVectorProperty(name = "displayed_frames_after", size = 10)
    ghost_opacity_gradient : bpy.props.FloatVectorProperty(name = "ghost_opacity_gradient", size = 21, default = 
                                                         (0.08 ,0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.45, 0.55, 0.70,
                                                          1.00, 
                                                         0.70, 0.55, 0.45, 0.35, 0.30, 0.25, 0.20, 0.15, 0.10,0.08))

    # mode : bpy.props.StringProperty(default = 'REL')
    mode : bpy.props.EnumProperty(items = [('DIS','Off','Disabled'), 
                                           ('REL','Object','Relative'), 
                                           ('ABS','World','Absolute'), 
                                           ('CAM','Camera ','Camera Relative')]
                                           , default = 'DIS', update = on_settings_update
                                           )
    target : bpy.props.EnumProperty(name="target",items=(('SELECTED','Selected only','Selected only'),
                                                         ('CHILD','Childs','Childs')),
                                                         default = 'SELECTED',
                                                         update = on_settings_update)
    stop_recursion : bpy.props.BoolProperty(default = False)
    onion_mode: bpy.props.EnumProperty(items = [('Frames','Frames','Frames'),
                                                ('Keyframes','WIP! Keyframes','Keyframes')
                                                ], 
                                       default = 'Frames',
                                       update=on_settings_update)
    

    filter_by_type = [
        ("ALL","All","Include all Keyframe types", "", 0),
        ("KEYFRAME","Keyframe", "Normal keyframe - e.g. for key poses","KEYTYPE_KEYFRAME_VEC",1),
        ("BREAKDOWN", "Breakdown", "A breakdown pose - e.g. for transitions between key poses","KEYTYPE_BREAKDOWN_VEC",2),
        ("MOVING_HOLD", "Moving Hold", "A keyframe that is part of a moving hold", "KEYTYPE_MOVING_HOLD_VEC", 3),
        ("EXTREME", "Extreme", "An 'extreme' pose, or some other purpose as needed", "KEYTYPE_EXTREME_VEC", 4),
        ("JITTER","Jitter", "A filler or baked keyframe for keying on ones, or some other purpose as needed", "KEYTYPE_JITTER_VEC", 5)]
    filter_kf_type : bpy.props.EnumProperty(items = filter_by_type, name = "Filter by Type", default = "ALL", update = on_settings_update)

    #pointers to grease pencil materials
    before_material : bpy.props.PointerProperty(type = bpy.types.Material)
    after_material : bpy.props.PointerProperty(type = bpy.types.Material)
    before_fill_material : bpy.props.PointerProperty(type = bpy.types.Material)
    after_fill_material : bpy.props.PointerProperty(type = bpy.types.Material)

    before_material_name : bpy.props.StringProperty(default = 'OS_mat_before')
    after_material_name : bpy.props.StringProperty(default = 'OS_mat_after')
    before_fill_material_name : bpy.props.StringProperty(default = 'OS_fill_mat_before')
    after_fill_material_name : bpy.props.StringProperty(default = 'OS_fill_mat_after')
    
    pinned_objects: bpy.props.StringProperty(default = '', update = on_settings_update)

    ###Onion skin default settings :
    cur_onion_mode :bpy.props.StringProperty(default = 'RELATIVE')
    cur_onion_factor : bpy.props.FloatProperty(default = 1.0,
                                               min = 0.0,
                                               max = 1.0,
                                               update=on_settings_update) 
    cur_ghost_before_range : bpy.props.IntProperty(default = 2 )
    cur_ghost_after_range : bpy.props.IntProperty(default = 2 )
    cur_before_color : bpy.props.FloatVectorProperty(default =(0.037, 0.098, 0.035))
    cur_after_color : bpy.props.FloatVectorProperty(default =(0.030, 0.021, 0.11))   
    cur_fill_before_color : bpy.props.FloatVectorProperty(default =(0.1450980007648468, 0.4196079969406128, 0.137254998087883))
    cur_fill_after_color : bpy.props.FloatVectorProperty(default =(0.1254899948835373, 0.08235300332307816, 0.5294119715690613)) 

    def get_pinned_objects_keys(self):
        return self.pinned_objects.split(',')

    # def set_materials(self, context):
    #     """Sets the materials to be used for ghost frames."""
    #     self.before_material = get_material_by_name("OS_mat_before")
    #     self.after_material = get_material_by_name("OS_mat_after")



class OS_GHOST_TRANSFORM_DATA(PropertyGroup):
    #Property group for ghost frame transform backup
    name : bpy.props.StringProperty()
    location : bpy.props.FloatVectorProperty(size = 3)
    rotation : bpy.props.FloatVectorProperty(size = 3)
    scale : bpy.props.FloatVectorProperty(size = 3)

def on_pin_update(self, context):
    
    '''add or removed object of pinned objects list'''
    if self.pinned is True:
        #get name of object self is attached to        
        if self.parent_name not in context.scene.osdata.pinned_objects:
            context.scene.osdata.pinned_objects += self.parent_name + ','
    else:
        if self.parent_name in context.scene.osdata.pinned_objects:
            context.scene.osdata.pinned_objects = context.scene.osdata.pinned_objects.replace( self.parent_name + ',', '')


class OS_SRC_GHOST_DATA(PropertyGroup):
    bl_label = "GP Onionskin Source Ghost Properties"
    bl_idname = "os_src_ob_data"
    parent_name : bpy.props.StringProperty(default = '')
    pinned : bpy.props.BoolProperty(default = False,update=on_pin_update)
    expanded : bpy.props.BoolProperty(default = False)
    ghost_before_range : bpy.props.IntProperty(default = 2, min = 0, soft_min = 0)
    ghost_after_range : bpy.props.IntProperty(default = 2, min = 0, soft_min = 0)
    previous_ghost_before_range : bpy.props.IntProperty(default = 2, min = 0, soft_min = 0)
    previous_ghost_after_range : bpy.props.IntProperty(default = 2, min = 0, soft_min = 0)
    src_in_front_value : bpy.props.BoolProperty(default = False)
    filter_by_type = [
        ("ALL","All","Include all Keyframe types", "", 0),
        ("KEYFRAME","Keyframe", "Normal keyframe - e.g. for key poses","KEYTYPE_KEYFRAME_VEC",1),
        ("BREAKDOWN", "Breakdown", "A breakdown pose - e.g. for transitions between key poses","KEYTYPE_BREAKDOWN_VEC",2),
        ("MOVING_HOLD", "Moving Hold", "A keyframe that is part of a moving hold", "KEYTYPE_MOVING_HOLD_VEC", 3),
        ("EXTREME", "Extreme", "An 'extreme' pose, or some other purpose as needed", "KEYTYPE_EXTREME_VEC", 4),
        ("JITTER","Jitter", "A filler or baked keyframe for keying on ones, or some other purpose as needed", "KEYTYPE_JITTER_VEC", 5)]
    filter_kf_type : bpy.props.EnumProperty(items = filter_by_type, name = "Filter by Type", default = "ALL", update = update_filter_kf_type)
    list_matrix_transform : bpy.props.CollectionProperty(type=OS_GHOST_TRANSFORM_DATA)

    def set_parent_name(self, value):
        self.parent_name = value

class OS_GHOST_DATA(PropertyGroup):
    bl_label = "GP Onionskin Ghost Properties"
    bl_idname = "os_ghost_data"
    #property group to store custom properties added elsewhere in code


classes = [    
    OS_OT_MOVE_KF,
    OS_OT_DELETE_FRAME_TRANSFORM,
    OS_OT_DELETE_TRANSFORMS,
    OS_OT_CLEAR,
    # OS_OT_GENERATE,
    # OS_OT_GENERATE_ABS,
    # OS_OT_GENERATE_LOC,
    # OS_OT_GENERATE_CAM,
    # OS_OT_USE_OS,
    # OS_OT_RESET_OPACITIES,
    OS_DATA,
    OS_GHOST_TRANSFORM_DATA,
    OS_SRC_GHOST_DATA,
    OS_GHOST_DATA
    
]

def register() :
    register_classes(classes)
    bpy.types.Scene.osdata = bpy.props.PointerProperty(type=OS_DATA)
    #set default before and after material


    #add handlers
    bpy.app.handlers.frame_change_post.append(frame_handler)
    bpy.app.handlers.render_init.append(pre_render)
    bpy.app.handlers.render_complete.append(post_render)
    bpy.app.handlers.render_cancel.append(post_render)
    bpy.app.handlers.depsgraph_update_post.append(scene_update_post)

    # bpy.types.GreasePencil.os_current_ghost_opacities = bpy.props.FloatVectorProperty(
    #     name = 'Opacity',
    #     default = ghost_opacity_gradient,
    #     min = 0.0,
    #     max = 1.0,
    #     soft_min = 0.0,
    #     soft_max = 1.0,
    #     size = len(ghost_opacity_gradient),
    #     update = update_custom_layer_opacity
    # )

    bpy.types.GreasePencil.os_src_ob_data = bpy.props.PointerProperty(type = OS_SRC_GHOST_DATA)
    bpy.types.GreasePencil.os_ghost_data = bpy.props.PointerProperty(type = OS_GHOST_DATA)

def unregister() :
    bpy.app.handlers.frame_change_post.remove(frame_handler)
    bpy.app.handlers.render_init.remove(pre_render)
    bpy.app.handlers.render_complete.remove(post_render)
    bpy.app.handlers.render_cancel.remove(post_render)
    bpy.app.handlers.depsgraph_update_post.remove(scene_update_post)
    unregister_classes(classes)
    del bpy.types.Scene.osdata
    del bpy.types.GreasePencil.os_src_ob_data
    del bpy.types.GreasePencil.os_ghost_data
