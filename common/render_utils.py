import bpy
import os
try:
    # import imageio
    from PIL import Image 
except:
    print('PIL not found. Trying to install it...')
    try:
        import pip
        pip.main(['install','pillow'])
        from PIL import Image 
        print('PIL installed successfully.')
    except:
        print('PIL could not be installed. Please install it manually.')
    pass

default_path= r'C:\Users\gerar\Documents\ANDARTA\render'

def get_viewport_gif(render_path=default_path,prefix='preview',frame_range = (0,10)):
    snapshots = []
    # for each frame, get a snapshot of the viewport
    #Temporary set context area to 'VIEW_3D' to get the right snapshot
    
    
    
    # for frame in range(frame_range[0],frame_range[1]):
    #     bpy.context.scene.frame_set(frame)
    #     window,area,region=get_window_area_region(context=bpy.context, area_type='VIEW_3D', region_type='WINDOW')
    #     with bpy.context.temp_override(window=window, area=area, region=region):
    #         snapshots.append(get_viewport_snapshot_file(render_path,prefix+str(frame),'.png'))
    snapshots = render_image_sequence(render_path,
                          prefix,
                          starting_frame=frame_range[0],
                          ending_frame=frame_range[1],
                          use_viewport=True)

    # and merge them as a gif
    return merge_pngs_as_gif(snapshots, name = prefix, duration = 0.5, render_path = render_path, remove_src = True)

def get_viewport_snapshot_file(render_path=default_path,prefix='preview',suffix='.png'):
    '''Get a snapshot of the viewport and save it to a file'''
    #Get a snapshot of the viewport
    bpy.ops.screen.screenshot_area(filepath = os.path.join(render_path, prefix + suffix))   

    return os.path.join(render_path, prefix + suffix)

def render_image_sequence (render_path=default_path,prefix='preview',
                            format='PNG',
                            starting_frame=0,
                            ending_frame=10,
                            use_viewport=False,
                            resolution_percentage=25 ):
    render_settings,scene_settings = save_render_settings()
    
    #render scene as images
    bpy.context.scene.render.image_settings.file_format = format
    bpy.context.scene.render.filepath = os.path.join(render_path, prefix)
    bpy.context.scene.render.resolution_percentage = resolution_percentage
    bpy.context.scene.frame_start = starting_frame
    bpy.context.scene.frame_end = ending_frame
    
    #Use viewport
    bpy.ops.render.render(use_viewport=use_viewport, write_still=True,animation=True)

    #get created images path
    images = [os.path.join(render_path, f) for f in os.listdir(render_path) if (f.endswith('.'+format.lower()) and f.startswith(prefix))]

    restore_render_settings(render_settings,scene_settings,)
    return images

def save_render_settings():
    '''Store render settings in a dic to restore them later'''
    render_settings = {}
    scene_settings = {}
    for args in bpy.context.scene.render.__dir__():
        if not args.startswith('_'):
            render_settings[args] = getattr(bpy.context.scene.render, args)
    for args in ['frame_start','frame_end']:
        scene_settings[args] = getattr(bpy.context.scene, args)
    return render_settings,scene_settings

def restore_render_settings(render_settings,scene_settings):
    '''Restore render settings from a dic'''
    for args in render_settings:
        try:
            setattr(bpy.context.scene.render, args, render_settings[args])
        except:#Some settings are read-only
            pass
    for args in scene_settings:
        setattr(bpy.context.scene, args, scene_settings[args])






def get_window_area_region(context=bpy.context, area_type='VIEW_3D', region_type=None):
    window = context.window_manager.windows[0]

    
    area = region = None
    for area_iter in window.screen.areas :
            if area_iter.type == area_type :
                area = area_iter
                if region_type is None:
                    break
                for region_iter in area.regions :
                    if region_iter.type == region_type:
                        region = region_iter
                        # saved_pers = area_view_3D.spaces[0].region_3d.view_perspective
                        break
                return window,area,region
        
    return window,area,region

def merge_pngs_as_gif(png_list,
                      name='default',
                      duration = 0.5,
                      render_path = default_path,
                      remove_src = True):
    #merge pngs as gif
    # png_list = [os.path.join(render_path, f) for f in os.listdir(render_path) if f.endswith('.png')]
    png_list.sort()
    images = []
    for png in png_list:
        #images.append(imageio.imread(png))
        #Usig pil
        images.append(Image.open(png))
    # imageio.mimsave(os.path.join(render_path, name+'.gif'), images, duration = duration)
    #create gif using PIL
    images[0].save(os.path.join(render_path, name+'.gif'),
                        save_all=True,
                        append_images=images[1:],
                        duration=duration,
                        #Infinity loop
                        loop=0
                        )

    if remove_src:
        for png in png_list:
            os.remove(png)
    return os.path.join(render_path, name+'.gif')