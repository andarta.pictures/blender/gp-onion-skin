import hashlib
hash_dict = {}

def has_hash_change(blender_object):
    """based on object type, calculate identification hash 
    and compare with occurence on hash_dict
    """
    match blender_object.bl_rna.identifier:
        #grease pencil frame
        case 'GPencilFrame':
            #calculate hash based on freme.strokes
            new_hash = hash(blender_object.strokes)
            #compare with hash_dict
            prev_hash = hash_dict.get(blender_object)
            if new_hash != prev_hash:                
                hash_dict[blender_object] = new_hash
                return True
            else:
                #if hash does not exist, add to hash_dict
               
                return False
        case _:
            return False